/**************************************************************************************************
  Filename:       advs.c
  Revised:        $Date: 2015-10-13 17:47:58 +0800 $
  Revision:       $Revision: 0 $

  Description:    All Advertising beacon data for WeiXin Yao Yi Yao
**************************************************************************************************/

#include "hal_mcu.h"
#include "advs.h"
#include "keyfobdemo.h"
#include "OSAL.h"
#include "gap.h"

const uint8 aBeacon_195777[] =
{
    0x02,   // length of first data structure (2 bytes excluding length byte)
    GAP_ADTYPE_FLAGS,   // AD Type = Flags
    0x1a,

    0x1B,   // length of second data structure (7 bytes excluding length byte)
    0xff, 0x4c, 0x00, 0x02, 0x15,
    0xFD, 0xA5, 0x06, 0x93, 0xA4, 0xE2, 0x4F, 0xB1, 0xAF, 0xCF, 0xC6, 0xEB, 0x07, 0x64, 0x78, 0x25,

    0x27, 0x11, //10001
    0xE9, 0xCF, //59855

    0xc5,
    0xBB,//30
};