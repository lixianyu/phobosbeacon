/**************************************************************************************************
  Filename:       knocking.c
  Revised:        $Date: 2014-09-09 10:15:58 +0800 (Tue, 9 Sep 2014) $
  Revision:       $Revision: 0 $

  Description:    When knocking 3 times Phobos
                  will wake up from PM3 to PM2.

**************************************************************************************************/

#include "hal_mcu.h"
#include "knocking.h"
#include "keyfobdemo.h"
#include "OSAL.h"

extern uint8 keyfobapp_TaskID;
void processKnockingInterruptP0_7 (void);
void processKnockingInterruptP1_7 (void);
/*
 * General-purpose I/O pins configured as inputs can be used to generate interrupts.
 */
#if 0
HAL_ISR_FUNCTION( knockingIsrP17, P1INT_VECTOR )
{
    HAL_ENTER_ISR();
    if (P1IFG & BV(7))
    {
        processKnockingInterruptP1_7();
    }
    /*
     Clear the CPU interrupt flag for Port_1
     PxIFG has to be cleared before PxIF
     */
    P1IFG = 0;
    P1IF = 0;
    HAL_EXIT_ISR();
}
#endif

HAL_ISR_FUNCTION( knockingIsrP07, P0INT_VECTOR )
{
    HAL_ENTER_ISR();
    if (P0IFG & BV(7))
    {
        processKnockingInterruptP0_7();
    }
    /*
     Clear the CPU interrupt flag for Port_0
     PxIFG has to be cleared before PxIF
     */
    P0IFG = 0;
    P0IF = 0;
    HAL_EXIT_ISR();
}

void knockingEnableP1_7(bool flag)
{
    if (flag)
    {
        P1SEL &= ~(BV(7));    /* Set pin function to GPIO */
        P1DIR |=  (BV(7));    /* Set pin direction to Output */
        P1_7 = 0;
        P1IEN |=  (BV(7));    /* Set P1_7 interrupt are enabled */
        PICTL |=  (BV(2));    /* Falling edge on input gives interrupt. Port 1,inputs 7 to 4 interrupt configuration */

        P0SEL &= ~(BV(7));    /* Set pin function to GPIO */
        P0DIR &= ~(BV(7));    /* Set pin direction to Input */
        P0_7 = 0;

        IEN2 |= (BV(4));   /* enable CPU interrupt for P1*/
        P0IFG = 0;
        P0IF = 0;
    }
    else
    {
        PICTL &= ~(BV(2));  /* ReSet the edge bit for P1*/
        IEN2 &= ~(BV(4));   /* diable CPU interrupt */
        EA = 0;
    }
}

void knockingEnableP0_7(bool flag)
{
    if (flag)
    {
        //TBD
        P1SEL &= ~(BV(7));    /* Set pin function to GPIO */
        P1DIR |= (BV(7));     /* Set pin direction to Output */
        P1_7 = 0;

        P0SEL &= ~(BV(7));    /* Set pin function to GPIO */
        P0DIR &= ~(BV(7));    /* Set pin direction to Input */
        P0IEN |= (BV(7));     /* Set P0_7 interrupt are enabled */
        PICTL |= (BV(0));     /* Set the edge bit to set falling edge to give int for P0*/

        IEN1 |= (BV(5));   /* enable CPU interrupt for P0*/

        P0IFG = 0;
        P0IF = 0;
    }
    else
    {
        PICTL &= ~(BV(0));   /* ReSet the edge bit for P0*/
        IEN1 &= ~(BV(5));    /* diable CPU interrupt */
        EA = 0;
    }
}

void processKnockingInterruptP0_7 (void)
{
    //osal_set_event( keyfobapp_TaskID, KFD_KNOCK_EVT );
    osal_start_timerEx( keyfobapp_TaskID, KFD_KNOCK_EVT, 50);
}

void processKnockingInterruptP1_7 (void)
{
    //osal_set_event( keyfobapp_TaskID, KFD_KNOCK_EVT );
}