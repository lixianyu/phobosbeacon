/**************************************************************************************************
  Filename:       advs.h
  Revised:        $Date: 2015-10-13 17:45:58 +0800 $
  Revision:       $Revision: 0 $

  Description:    All Advertising beacon data for WeiXin Yao Yi Yao
**************************************************************************************************/

#ifndef ADVS_H
#define ADVS_H

#include "hal_types.h"

extern const uint8 aBeacon_195777[];
#endif
